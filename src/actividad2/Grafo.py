from Vertice import *

class Grafo:
    vertices={}
    def agregarVertice(self,vertice):
        if isinstance(vertice,Vertice) and vertice.nombre not in self.vertices:
            self.vertices[vertice.nombre]=vertice
            return True
        else:
            return False
    def agregarArista(self, u, v):
        if u in self.vertices and v in self.vertices:
            for key, value in self.vertices.items():
                if key ==u:
                    value.agregarVecino(v)
                if key==v:
                    value.agregarVecino(u)
            return True
        else:
            return False
    # se agrega el metodo BFS

    def imprimirGrafo(self):
        for key in sorted(list(self.vertices.keys())):
            print("Vertice "+ key + "sus vecinos son" + str (self.vertices[key].vecinos))
            
