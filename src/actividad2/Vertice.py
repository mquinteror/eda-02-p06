#clase vertice
class Vertice:
    def __init__(self,n):
        self.nombre=n
        self.vecinos=list()
        self.distancia=9999
        self.color='white'
        self.pred=-1
        
    def agregarVecino(self,v):
        if v not in self.vecinos:
            self.vecinos.append(v)
            self.vecinos.sort()
