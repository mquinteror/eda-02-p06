#importando paquetes necesarios
from Vertice import *
from Grafo import *
# clase controladora
class Controladora:
    def main(self):
        print("mensaje")
        #se crea un obj 'g' de la clase grafo, el grafo
        g=Grafo()
        #se crea un opbjeto 'a' de la clase Vertice, un vertice
        a= Vertice ('A')
        #se agrega el vertice a al grafo
        g.agregarVertice(a)
        
        #esta estructura  de repeticion es para agregar
        #todos los vertices, y no hacerlo uno a uno
        for i in range(ord('A'), ord ('k')):
            g.agregarVertice(Vertice(chr(i)))
            
        #crea la lista con las aristas del grafo
        edges=['AB', 'AE', 'BF', 'CG', 'DE', 'DH', 'EH', 'FG', 'FI' , 'FJ',  'GJ']
        
        #SE agregan las aristas al grafo
        for edge in edges:
            g.agregarArista(edge[:1], edge[1:])
            #imprim el grafo con lista de adyacencia
            g.imprimirGrafo()
